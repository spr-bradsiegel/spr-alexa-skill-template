cd lambda
npm install
zip -r ../lambda.zip *
cd ..
aws lambda update-function-code --function-name {insert lambda function name here} --zip-file fileb://lambda.zip
rm lambda.zip
